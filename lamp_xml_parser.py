#! /usr/bin/python3.6
import os
import cv2
import xml.etree.ElementTree as ET
from copy import deepcopy

import argparse

class Track:
    def __init__(self, xml_node=None):
        self.tracked_elements = dict()
        self.label = ""
        if xml_node.attrib['label'] is not None:
            self.label = xml_node.attrib['label']

class Box:
    def __init__(self, xml_node=None, label=""):
        self.extra_attributes = dict()
        self.label = label
        if xml_node is not None:
            self.attributes = deepcopy(xml_node.attrib)
            for child in xml_node:
                # Each box might contain attributes of the form
                # <attribute name="ATTRIBUTE">VALUE</attribute>
                if child.attrib['name'] is not None:
                    self.extra_attributes[child.attrib['name']] = child.text
        else:
            self.attributes = dict()

class Polygon:
    def __init__(self, xml_node):
        self.attributes = deepcopy(xml_node.attrib)

class CVATDocument:
    def __init__(self, filepath=''):
        self.tracks = list()
        self.max_frame = 0
        self.doc_tree = None
        if filepath is not '':
            self.doc_tree = ET.parse(filepath)

    def open_doc(self, filepath):
        self.doc_tree = ET.parse(filepath)

    def parse(self):
        if self.doc_tree is None:
            raise ValueError("Doc Tree is none")
        root = self.doc_tree.getroot()
        self.max_frame = 0

        for child in root:
            if child.tag == 'track':
                new_track = Track(child)
                for node in child:
                    frame = int(node.attrib['frame'])
                    self.max_frame = max(self.max_frame, frame)
                    new_track.tracked_elements[frame] = parse_node(node, new_track.label)
                self.tracks.append(new_track)

    """
    MOT-Like Format
    
    <frame>, <id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <bb_class>, <left-signal>, <brake>, <right-signal>, <side>, <image_name>, <occlusion>
    1, 3, 794.27, 247.59, 71.245, 174.88, lamp, false, false, false, front, false
    1, 6, 1648.1, 119.61, 66.504, 163.24, lamp, true, true, false, rear, true
    1, 8, 875.49, 399.98, 95.303, 233.93, lamp, true, true, true, rear, false
    """
    def to_format(self, filepath='', imagepath='', imgextension='.jpg'):
        output_file = None
        if not filepath == '':
            output_file = open(filepath, "w")
        if not imagepath == '' and not os.path.exists(imagepath):
            print("Invalid Image Path")
            return

        uniqueId = 0
        for frame in range(0, (self.max_frame + 1)):
            for track in self.tracks:
                if frame in track.tracked_elements and track.label == "lamp":
                    frame_info = track.tracked_elements[frame].attributes
                    box_info = track.tracked_elements[frame].extra_attributes
                    bb_left = int(float(frame_info['xtl']))
                    bb_top = int(float(frame_info['ytl']))
                    bb_width = int(float(frame_info['xbr']) - bb_left)
                    bb_height = int(float(frame_info['ybr']) - bb_top)
                    bb_class = track.label
                    bb_brake = box_info['brake']
                    bb_left_signal = box_info['left-signal']
                    bb_right_signal = box_info['right-signal']
                    bb_side = box_info['side']
                    bb_image_name = "{0}{1}".format(frame, imgextension)
                    bb_occluded = frame_info['occluded']

                    formatted_line = "{0}, {1}, {2}, {3}, {4}, " \
                                     "{5}, {6}, {7}, {8}, {9}, {10}, {11}, {12} \n".format(
                                                                        frame,
                                                                        self.tracks.index(track),
                                                                        bb_left,
                                                                        bb_top,
                                                                        bb_width,
                                                                        bb_height,
                                                                        bb_class,
                                                                        bb_left_signal,
                                                                        bb_brake,
                                                                        bb_right_signal,
                                                                        bb_side,
                                                                        bb_image_name,
                                                                        bb_occluded)
                    if output_file is not None:
                        output_file.write(formatted_line)
                    print(formatted_line)

                    if not imagepath == '':
                        cur_img_path = os.path.join(imagepath, bb_image_name)
                        if os.path.exists(cur_img_path):
                            cv_image = cv2.imread(cur_img_path)
                            cv2.rectangle(cv_image,
                                          (bb_left, bb_top),
                                          (bb_left+bb_width, bb_top+bb_height),
                                          (255, 0, 0),
                                          2
                                          )
                            cv2.imshow("test", cv_image)
                            cv2.waitKey(5)




def parse_node(node, label=""):
    if node.tag == 'box':
        return Box(node, label)
    if node.tag == 'polygon':
        return Polygon(node, label)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--outfile", help="Path to the desired output file. If not given, "
                                                "output is printed to the console.", default='')
    parser.add_argument("-d", "--display", help="Path to the images corresponding to this dataset. If not given, "
                                                "display is not shown.", default='')
    parser.add_argument("xmlfile", help="Path to the XML file to parse")

    args = parser.parse_args()
    doc = None

    print("InputFile: ", args.xmlfile)
    print("Output File: ", args.outfile)
    print("Images Path: ", args.display)

    if args.xmlfile is not None:
        doc = CVATDocument(args.xmlfile)
        doc.parse()
        doc.to_format(args.outfile, args.display, '.jpg')